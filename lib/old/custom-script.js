(function ($) {
	// Scroll Event
	$(window).on('scroll', function () {
		var windowTop = $(window).scrollTop(),
    	windowWidth = $(window).width(),
        header = $('#header'),
        navContainer = $('#main-nav-container'),
        carbutton = $(".cart-button-2"),
        navContainerHeight = navContainer.height(),
        navDist = navContainer.offset().top,
        headerHeight = (header.height() - navContainerHeight);
	       	
	    if (windowTop >= navDist && windowTop > headerHeight && windowWidth > 768) {
	    	$(".cart-button-2").addClass('ledisplay-menu');
	    } else {
	    	$(".cart-button-2").removeClass('ledisplay-menu');
	    }
	        	
	});

})(jQuery);