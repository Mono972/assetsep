import './jquery.hoverIntent.min';
import './bootstrap-datepicker.min';
import './custom-script';
import './simplePagination';
import './dirPagination';
// import './wow';

// new WOW().init();
//
// (function($) {
//
//   /* PRELODER */
//   $(window).load(function(){
//     $('.preloader').fadeOut(200);
//   });
//
//   $(document).on( 'click', 'a', function(){
//     if( $(this).hasClass('.no-loader') && $(this).attr('href')[0] != '#' && $(this).attr('href') !== 'javascript:;' ){
//       $('.preloader').fadeIn(100);
//     }
//   });
//
//
// })( jQuery );

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');


/*var LHCChatOptions = {};
LHCChatOptions.opt = {widget_height:340,widget_width:300,popup_height:520,popup_width:500,domain:'zona572.com'};
(function() {
  var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
  var referrer = (document.referrer) ? encodeURIComponent(document.referrer.substr(document.referrer.indexOf('://')+1)) : '';
  var location  = (document.location) ? encodeURIComponent(window.location.href.substring(window.location.protocol.length)) : '';
  po.src = '//ferreteriaonlinehoyostools.com/soporte/chat/index.php/esp/chat/getstatus/(click)/internal/(position)/bottom_right/(ma)/br/(top)/350/(units)/pixels/(leaveamessage)/true?r='+referrer+'&l='+location;

  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
})();*/


(function() {
  (function(I,n,f,o,b,i,p){
    I[b]=I[b]||function(){(I[b].q=I[b].q||[]).push(arguments)};
    I[b].t=1*new Date();i=n.createElement(f);i.async=1;i.src=o;
    p=n.getElementsByTagName(f)[0];p.parentNode.insertBefore(i,p)})
    (window,document,'script','https://livechat.infobip.com/widget.js','liveChat');
    
    liveChat('init', '25e675ca-42ff-4af0-ad7c-9b70cc6001bc');
})();

$( ".aceptar-cookies" ).click(function( event ) {
  event.preventDefault();
  $(".info-cookie").remove();
});

$('#scroll-top').on('click', function (e) {
  $('html, body').animate({
    'scrollTop': 0
  }, 1200);
  e.preventDefault();
});

$(window).scroll(function(){
  if ($(window).scrollTop() >= 45) {
    $('.navbar').addClass('fixed-top');
  }
  else {
    $('.navbar').removeClass('fixed-top');
  }
});

// function setModalMaxHeight(element) {
//   this.$element     = $(element);
//   var dialogMargin  = $(window).width() > 767 ? 62 : 22;
//   var contentHeight = $(window).height() - dialogMargin;
//   var headerHeight  = this.$element.find('.modal-header').outerHeight() || 2;
//   var footerHeight  = this.$element.find('.modal-footer').outerHeight() || 2;
//   var maxHeight     = contentHeight - (headerHeight + footerHeight);
//
//   this.$element
//     .find('.modal-content').css({
//     'overflow': 'hidden'
//   });
//
//   this.$element
//     .find('.modal-body').css({
//     'max-height': maxHeight,
//     'overflow-y': 'auto'
//   });
// }

$('.modal').on('show.bs.modal', function() {
  $(this).show();
  setModalMaxHeight(this);
});

$(window).resize(function() {
  if ($('.modal.in').length != 0) {
    setModalMaxHeight($('.modal.in'));
  }
});

// $('.modal-backdrop').remove();

//Este código permitiá mostrar items en la lista de categorias dependiendo del tamaño de la pantalla
/*setTimeout(function(){
  var TotalCategorias = $(".panel");

  var maxi_cats = 0;
  var diff_ancho=0;
  var alto_ini=$(window).height();
  var ancho_ini=$(window).width();

  if($(window).width() >= 1200){
    maxi_cats = 7;
    $( ".acc-nuestros-productos .panel:gt("+maxi_cats+")" ).css( "display", "none" );
    $( ".ver-mas-cat" ).css( "display", "none" );

    if( TotalCategorias.length  > maxi_cats + 1){
      $( ".ver-mas-cat" ).css( "display", "block" );
    }

  }
  if($(window).width() < 1200){
    maxi_cats = 6;
    $( ".acc-nuestros-productos .panel:gt("+maxi_cats+")" ).css( "display", "none" );
    $( ".ver-mas-cat" ).css( "display", "none" );

    if( TotalCategorias.length  > maxi_cats + 1){
      $( ".ver-mas-cat" ).css( "display", "block" );
    }
  }
  if($(window).width() < 993){
    maxi_cats = 2;
    $( ".acc-nuestros-productos .panel:gt("+maxi_cats+")" ).css( "display", "none" );
    $( ".ver-mas-cat" ).css( "display", "none" );

    if( TotalCategorias.length  > maxi_cats + 1){
      $( ".ver-mas-cat" ).css( "display", "none" );
    }
  }

  $(window).resize(function(){
    //aqui el codigo que se ejecutara cuando se redimencione la ventana
    var alto=$(window).height();
    var ancho=$(window).width();


    diff_ancho = ancho_ini - ancho;

    if (diff_ancho < 0) {
      diff_ancho*=-1;
    }

    if (diff_ancho > 20) {

      ancho_ini=$(window).width();

      $( ".ver-menos-cat" ).css( "display", "none" );
      $( ".ver-mas-cat" ).css( "display", "block" );

      if(ancho >= 1200){
        maxi_cats = 9;
        $( ".acc-nuestros-productos .panel" ).css( "display", "block" );
        $( ".acc-nuestros-productos .panel:gt("+maxi_cats+")" ).css( "display", "none" );

        $( ".ver-mas-cat" ).css( "display", "none" );
        if( TotalCategorias.length > maxi_cats + 1){
          $( ".ver-mas-cat" ).css( "display", "block" );
        }
      }
      if(ancho < 1200){
        maxi_cats = 6;
        $( ".acc-nuestros-productos .panel" ).css( "display", "block" );
        $( ".acc-nuestros-productos .panel:gt("+maxi_cats+")" ).css( "display", "none" );

        $( ".ver-mas-cat" ).css( "display", "none" );
        if( TotalCategorias.length > maxi_cats + 1){
          $( ".ver-mas-cat" ).css( "display", "block" );
        }
      }
      if(ancho < 993){
        maxi_cats = 2;
        $( ".acc-nuestros-productos .panel:gt("+maxi_cats+")" ).css( "display", "none" );

        $( ".ver-mas-cat" ).css( "display", "none" );
        if( TotalCategorias.length > maxi_cats + 1){
          $( ".ver-mas-cat" ).css( "display", "block" );
        }
      }



    }

  })

  $( ".ver-mas-cat" ).click(function() {
    $( ".acc-nuestros-productos .panel" ).show(500);
    $( ".ver-mas-cat" ).css( "display", "none" );
    $( ".ver-menos-cat" ).css( "display", "block" );

  });

  $( ".ver-menos-cat" ).click(function() {
    $( ".acc-nuestros-productos .panel:gt("+ maxi_cats+")" ).hide(500);
    $( ".ver-menos-cat" ).css( "display", "none" );
    $( ".ver-mas-cat" ).css( "display", "block" );
  });
},500);
*/
//Este es el mismo código pero minicado, que estava en el archivo all.js
/*
var TotalCategorias=$(".acc-nuestros-productos .panel"),maxi_cats=0,diff_ancho=0,alto_ini=$(window).height(),ancho_ini=$(window).width();$(window).width()>=1200&&(maxi_cats=10,$(".acc-nuestros-productos .panel:gt("+maxi_cats+")").css("display","none"),TotalCategorias.length<10&&$(".ver-mas-cat").css("display","none")),$(window).width()<1200&&(maxi_cats=7,$(".acc-nuestros-productos .panel:gt("+maxi_cats+")").css("display","none"),TotalCategorias.length<7&&$(".ver-mas-cat").css("display","none")),$(window).width()<993&&(maxi_cats=4,$(".acc-nuestros-productos .panel:gt("+maxi_cats+")").css("display","none"),TotalCategorias.length<4&&$(".ver-mas-cat").css("display","none")),$(window).resize(function(){var e=($(window).height(),$(window).width());diff_ancho=ancho_ini-e,0>diff_ancho&&(diff_ancho*=-1),diff_ancho>20&&(ancho_ini=$(window).width(),$(".ver-menos-cat").css("display","none"),$(".ver-mas-cat").css("display","block"),e>=1200&&(maxi_cats=10,$(".acc-nuestros-productos .panel").css("display","block"),$(".acc-nuestros-productos .panel:gt("+maxi_cats+")").css("display","none"),TotalCategorias.length<10&&$(".ver-mas-cat").css("display","none")),1200>e&&(maxi_cats=7,$(".acc-nuestros-productos .panel").css("display","block"),$(".acc-nuestros-productos .panel:gt("+maxi_cats+")").css("display","none"),TotalCategorias.length<7&&$(".ver-mas-cat").css("display","none")),993>e&&(maxi_cats=4,$(".acc-nuestros-productos .panel:gt("+maxi_cats+")").css("display","none"),TotalCategorias.length<4&&$(".ver-mas-cat").css("display","none")))}),$(".ver-mas-cat").click(function(){$(".acc-nuestros-productos .panel").show(500),$(".ver-mas-cat").css("display","none"),$(".ver-menos-cat").css("display","block")}),$(".ver-menos-cat").click(function(){$(".acc-nuestros-productos .panel:gt("+maxi_cats+")").hide(500),$(".ver-menos-cat").css("display","none"),$(".ver-mas-cat").css("display","block")});
* */
function initMainSlider() {


  var fixSliderForMobile = function () {
    var winWidth = $(window).width();

    if (winWidth <= 767 && $('#slider-rev-container').length) {
      var revSliderHeight = $('#slider-rev').height();
      console.log(revSliderHeight);
      $('.slider-position').css('padding-top', revSliderHeight);
      $('.main-content').css('position', 'static');
    } else {
      $('.slider-position').css('padding-top', 0);
      $('.main-content').css('position', 'relative');
    }
  };

}

initMainSlider();

$('#btn-canasta').on('click', function(){
  $('div.modal-backdrop').remove();
});

$('#btn-clean-filters').on('click', function(){
  $('#filter-wrapper').find('input:checked').click();
});

var vistaMultipleProductos = '.btn-vista1 a';

function cambiarElementosListado() {
  var contenedorItem = $('.opcion-vista');
  contenedorItem.each(function(i){
    $(this).find('.precio-producto-box .boton-acciones-producto').insertAfter($(this).find('.col-md-3'));
  });
}

function cambiarElementosNoListados() {
  var contenedorItem = $('.opcion-vista');

  contenedorItem.each(function(i){
    $(this).find('.boton-acciones-producto').insertAfter($(this).find('.precio-producto-box .precio-productos-item'));
  });
  console.log('precio-producto-box');
}

// function cambiarElementos() {
//   var contenedorItem = $('.opcion-vista');

//   contenedorItem.each(function(i){
//     $(this).find('.precio-producto-box .iconos-acciones-box').prependTo($(this).find('.producto-img-box'));
//     $(this).find('.ratings-container').appendTo($(this).find('.producto-img-box'));
//   });
// }

$('.vistas-pp').on('click', 'ul li a', function () {
  $(this).parent().addClass('activa').siblings().removeClass('activa');
});

$('.btn-vista1 a').on('click', function(){
  vistaMultipleProductos = '.btn-vista1 a';
  var contenedorItem = $('.opcion-vista');

  $('.opcion-vista').removeClass().addClass('row opcion-vista vista-1');
  cambiarElementosNoListados()

  // contenedorItem.each(function(i){
  //   $(this).find('.iconos-acciones-box').prependTo($(this).find('.precio-producto-box'));
  //   $(this).find('.ratings-container').prependTo($(this).find('.precio-producto-box'));
  // });
});


$('.btn-vista2 a').on('click', function(){
  vistaMultipleProductos = '.btn-vista2 a';

  $('.opcion-vista').removeClass().addClass('opcion-vista vista-2 col-md-6');
  cambiarElementosNoListados()
  // cambiarElementos();
});


$('.btn-vista3 a').on('click', function(){
  vistaMultipleProductos = '.btn-vista3 a';

  $('.opcion-vista').removeClass().addClass('opcion-vista vista-3 col-md-4');
  cambiarElementosNoListados();
  // cambiarElementos();
});

$('.btn-vista4 a').on('click', function(){
  vistaMultipleProductos = '.btn-vista4 a';

  $('.opcion-vista').removeClass().addClass('opcion-vista vista-4 col-md-3');
  cambiarElementosNoListados();
  // cambiarElementos();
});

$('.btn-vista5 a').on('click', function(){
  vistaMultipleProductos = '.btn-vista5 a';

  $('.opcion-vista').removeClass().addClass('opcion-vista vista-5 col-md-2');
  cambiarElementosNoListados();
  // cambiarElementos();
});

$('.btn-listado a').on('click', function(){
  vistaMultipleProductos = '.btn-listado a';

  $('.opcion-vista').removeClass().addClass('opcion-vista listado');
  cambiarElementosListado();
});

$('#contactProgrammedDelivery').on('changeDate', function(ev){
  $(this).datepicker('hide');
});

$('#contactProgrammedDelivery').datepicker().on('changeDate', function (e) {
  if(e.viewMode === 'days') {
    $(this).blur();
  }
});

$('#btnGuardarInfoTunel').on('click', function (e) {
  $('html, body').animate({
    'scrollTop': 0
  }, 300);
  e.preventDefault();
});

$('#incidenceDate').on('changeDate', function(ev){
  $(this).datepicker('hide');
});

$('#incidenceDate').datepicker().on('changeDate', function (e) {
  if(e.viewMode === 'days') {
    $(this).blur();
  }
});


